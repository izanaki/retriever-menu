/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "ngx-cookie";
export class RetrieverMenuService {
    /**
     * @param {?} http
     * @param {?} _cookieService
     */
    constructor(http, _cookieService) {
        this.http = http;
        this._cookieService = _cookieService;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._cookieService.get('token')
            })
        };
    }
    /**
     * @return {?}
     */
    get authHost() { return this._authHost; }
    /**
     * @param {?} authHost
     * @return {?}
     */
    set authHost(authHost) { this._authHost = authHost; }
    /**
     * @return {?}
     */
    get playlistsLink() { return this._playlistsLink; }
    /**
     * @param {?} playlistsLink
     * @return {?}
     */
    set playlistsLink(playlistsLink) { this._playlistsLink = playlistsLink; }
    /**
     * @return {?}
     */
    get templatesLink() { return this._templatesLink; }
    /**
     * @param {?} templatesLink
     * @return {?}
     */
    set templatesLink(templatesLink) { this._templatesLink = templatesLink; }
    /**
     * @return {?}
     */
    get savedTemplatesLink() { return this._savedTemplatesLink; }
    /**
     * @param {?} savedTemplatesLink
     * @return {?}
     */
    set savedTemplatesLink(savedTemplatesLink) { this._savedTemplatesLink = savedTemplatesLink; }
    /**
     * @return {?}
     */
    get mediaLibLink() { return this._mediaLibLink; }
    /**
     * @param {?} mediaLibLink
     * @return {?}
     */
    set mediaLibLink(mediaLibLink) { this._mediaLibLink = mediaLibLink; }
    /**
     * @return {?}
     */
    get appsLink() { return this._appsLink; }
    /**
     * @param {?} appsLink
     * @return {?}
     */
    set appsLink(appsLink) { this._appsLink = appsLink; }
    /**
     * @return {?}
     */
    get devicesLink() { return this._devicesLink; }
    /**
     * @param {?} devicesLink
     * @return {?}
     */
    set devicesLink(devicesLink) { this._devicesLink = devicesLink; }
    /**
     * @return {?}
     */
    get acctSettingsLink() { return this._acctSettingsLink; }
    /**
     * @param {?} acctSettingsLink
     * @return {?}
     */
    set acctSettingsLink(acctSettingsLink) { this._acctSettingsLink = acctSettingsLink; }
    /**
     * @return {?}
     */
    get switchOrgLink() { return this._switchOrgLink; }
    /**
     * @param {?} switchOrgLink
     * @return {?}
     */
    set switchOrgLink(switchOrgLink) { this._switchOrgLink = switchOrgLink; }
    /**
     * @return {?}
     */
    get userSettingsLink() { return this._userSettingsLink; }
    /**
     * @param {?} userSettingsLink
     * @return {?}
     */
    set userSettingsLink(userSettingsLink) { this._userSettingsLink = userSettingsLink; }
    /**
     * @return {?}
     */
    get signoutLink() { return this._signoutLink; }
    /**
     * @param {?} signoutLink
     * @return {?}
     */
    set signoutLink(signoutLink) { this._signoutLink = signoutLink; }
    /**
     * @return {?}
     */
    get loginLink() { return this._loginLink; }
    /**
     * @param {?} loginLink
     * @return {?}
     */
    set loginLink(loginLink) { this._loginLink = loginLink; }
    /**
     * @return {?}
     */
    get authKey() { return this._authKey; }
    /**
     * @param {?} authKey
     * @return {?}
     */
    set authKey(authKey) { this._authKey = authKey; }
    /**
     * @return {?}
     */
    get user() { return this._user; }
    /**
     * @param {?} user
     * @return {?}
     */
    set user(user) { this._user = user; }
    /**
     * @return {?}
     */
    verifyUser() {
        return this.http
            .get(this.authHost + 'verify', this.httpOptions);
    }
}
RetrieverMenuService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
RetrieverMenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: CookieService }
];
/** @nocollapse */ RetrieverMenuService.ngInjectableDef = i0.defineInjectable({ factory: function RetrieverMenuService_Factory() { return new RetrieverMenuService(i0.inject(i1.HttpClient), i0.inject(i2.CookieService)); }, token: RetrieverMenuService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._authHost;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._playlistsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._templatesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._savedTemplatesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._mediaLibLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._appsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._devicesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._acctSettingsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._switchOrgLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._userSettingsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._signoutLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._loginLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._authKey;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._user;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype.httpOptions;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._cookieService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3JldHJpZXZlci1tZW51LyIsInNvdXJjZXMiOlsibGliL3JldHJpZXZlci1tZW51LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUM3RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sWUFBWSxDQUFDOzs7O0FBTXpDLE1BQU0sT0FBTyxvQkFBb0I7Ozs7O0lBOEUvQixZQUFvQixJQUFnQixFQUFVLGNBQTZCO1FBQXZELFNBQUksR0FBSixJQUFJLENBQVk7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQVBuRSxnQkFBVyxHQUFHO1lBQ3BCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDdkIsY0FBYyxFQUFHLGtCQUFrQjtnQkFDbkMsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7YUFDOUQsQ0FBQztTQUNILENBQUM7SUFFNkUsQ0FBQzs7OztJQTNFaEYsSUFBSSxRQUFRLEtBQU0sT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDMUMsSUFBSSxRQUFRLENBQUUsUUFBZ0IsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJOUQsSUFBSSxhQUFhLEtBQU0sT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDcEQsSUFBSSxhQUFhLENBQUUsYUFBcUIsSUFBSSxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJbEYsSUFBSSxhQUFhLEtBQU0sT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDcEQsSUFBSSxhQUFhLENBQUUsYUFBcUIsSUFBSSxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJbEYsSUFBSSxrQkFBa0IsS0FBTSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7Ozs7O0lBQzlELElBQUksa0JBQWtCLENBQUUsa0JBQTBCLElBQUksSUFBSSxDQUFDLG1CQUFtQixHQUFHLGtCQUFrQixDQUFDLENBQUMsQ0FBQzs7OztJQUl0RyxJQUFJLFlBQVksS0FBTSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDOzs7OztJQUNsRCxJQUFJLFlBQVksQ0FBRSxZQUFvQixJQUFJLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQzs7OztJQUk5RSxJQUFJLFFBQVEsS0FBTSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzs7OztJQUMxQyxJQUFJLFFBQVEsQ0FBRSxRQUFnQixJQUFJLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQzs7OztJQUk5RCxJQUFJLFdBQVcsS0FBTSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDOzs7OztJQUNoRCxJQUFJLFdBQVcsQ0FBRSxXQUFtQixJQUFJLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQzs7OztJQUkxRSxJQUFJLGdCQUFnQixLQUFNLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDMUQsSUFBSSxnQkFBZ0IsQ0FBRSxnQkFBd0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDOzs7O0lBSTlGLElBQUksYUFBYSxLQUFNLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Ozs7O0lBQ3BELElBQUksYUFBYSxDQUFFLGFBQXFCLElBQUksSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDOzs7O0lBSWxGLElBQUksZ0JBQWdCLEtBQU0sT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDOzs7OztJQUMxRCxJQUFJLGdCQUFnQixDQUFFLGdCQUF3QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJOUYsSUFBSSxXQUFXLEtBQU0sT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDaEQsSUFBSSxXQUFXLENBQUUsV0FBbUIsSUFBSSxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJMUUsSUFBSSxTQUFTLEtBQU0sT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDNUMsSUFBSSxTQUFTLENBQUUsU0FBaUIsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUM7Ozs7SUFJbEUsSUFBSSxPQUFPLEtBQU0sT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFDeEMsSUFBSSxPQUFPLENBQUUsT0FBZSxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQzs7OztJQUkxRCxJQUFJLElBQUksS0FBTSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzs7OztJQUNsQyxJQUFJLElBQUksQ0FBRSxJQUFtQixJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQzs7OztJQVdyRCxVQUFVO1FBQ1IsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7O1lBdEZGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQU5PLFVBQVU7WUFDVixhQUFhOzs7Ozs7OztJQVFuQix5Q0FBMEI7Ozs7O0lBSzFCLDhDQUErQjs7Ozs7SUFLL0IsOENBQStCOzs7OztJQUsvQixtREFBb0M7Ozs7O0lBS3BDLDZDQUE4Qjs7Ozs7SUFLOUIseUNBQTBCOzs7OztJQUsxQiw0Q0FBNkI7Ozs7O0lBSzdCLGlEQUFrQzs7Ozs7SUFLbEMsOENBQStCOzs7OztJQUsvQixpREFBa0M7Ozs7O0lBS2xDLDRDQUE2Qjs7Ozs7SUFLN0IsMENBQTJCOzs7OztJQUszQix3Q0FBeUI7Ozs7O0lBS3pCLHFDQUE2Qjs7Ozs7SUFJN0IsMkNBS0U7Ozs7O0lBRVUsb0NBQXdCOzs7OztJQUFFLDhDQUFxQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cEhlYWRlcnN9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7Q29va2llU2VydmljZX0gZnJvbSAnbmd4LWNvb2tpZSc7XG5pbXBvcnQge1JldHJpZXZlclVzZXJ9IGZyb20gJy4vaW50ZXJmYWNlcy91c2VyJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUmV0cmlldmVyTWVudVNlcnZpY2Uge1xuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIGF1dGggc2VydmVyIC8vXG4gIHByaXZhdGUgX2F1dGhIb3N0OiBzdHJpbmc7XG4gIGdldCBhdXRoSG9zdCAoKSB7IHJldHVybiB0aGlzLl9hdXRoSG9zdDsgfVxuICBzZXQgYXV0aEhvc3QgKGF1dGhIb3N0OiBzdHJpbmcpIHsgdGhpcy5fYXV0aEhvc3QgPSBhdXRoSG9zdDsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9wbGF5bGlzdHNMaW5rOiBzdHJpbmc7XG4gIGdldCBwbGF5bGlzdHNMaW5rICgpIHsgcmV0dXJuIHRoaXMuX3BsYXlsaXN0c0xpbms7IH1cbiAgc2V0IHBsYXlsaXN0c0xpbmsgKHBsYXlsaXN0c0xpbms6IHN0cmluZykgeyB0aGlzLl9wbGF5bGlzdHNMaW5rID0gcGxheWxpc3RzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF90ZW1wbGF0ZXNMaW5rOiBzdHJpbmc7XG4gIGdldCB0ZW1wbGF0ZXNMaW5rICgpIHsgcmV0dXJuIHRoaXMuX3RlbXBsYXRlc0xpbms7IH1cbiAgc2V0IHRlbXBsYXRlc0xpbmsgKHRlbXBsYXRlc0xpbms6IHN0cmluZykgeyB0aGlzLl90ZW1wbGF0ZXNMaW5rID0gdGVtcGxhdGVzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9zYXZlZFRlbXBsYXRlc0xpbms6IHN0cmluZztcbiAgZ2V0IHNhdmVkVGVtcGxhdGVzTGluayAoKSB7IHJldHVybiB0aGlzLl9zYXZlZFRlbXBsYXRlc0xpbms7IH1cbiAgc2V0IHNhdmVkVGVtcGxhdGVzTGluayAoc2F2ZWRUZW1wbGF0ZXNMaW5rOiBzdHJpbmcpIHsgdGhpcy5fc2F2ZWRUZW1wbGF0ZXNMaW5rID0gc2F2ZWRUZW1wbGF0ZXNMaW5rOyB9XG5cbiAgLy8gVXNlZCB0byBkaXJlY3QgaHR0cCBjYWxscyB0byBwbGF5bGlzdHMgc2VydmVyIC8vXG4gIHByaXZhdGUgX21lZGlhTGliTGluazogc3RyaW5nO1xuICBnZXQgbWVkaWFMaWJMaW5rICgpIHsgcmV0dXJuIHRoaXMuX21lZGlhTGliTGluazsgfVxuICBzZXQgbWVkaWFMaWJMaW5rIChtZWRpYUxpYkxpbms6IHN0cmluZykgeyB0aGlzLl9tZWRpYUxpYkxpbmsgPSBtZWRpYUxpYkxpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfYXBwc0xpbms6IHN0cmluZztcbiAgZ2V0IGFwcHNMaW5rICgpIHsgcmV0dXJuIHRoaXMuX2FwcHNMaW5rOyB9XG4gIHNldCBhcHBzTGluayAoYXBwc0xpbms6IHN0cmluZykgeyB0aGlzLl9hcHBzTGluayA9IGFwcHNMaW5rOyB9XG5cbiAgLy8gVXNlZCB0byBkaXJlY3QgaHR0cCBjYWxscyB0byBwbGF5bGlzdHMgc2VydmVyIC8vXG4gIHByaXZhdGUgX2RldmljZXNMaW5rOiBzdHJpbmc7XG4gIGdldCBkZXZpY2VzTGluayAoKSB7IHJldHVybiB0aGlzLl9kZXZpY2VzTGluazsgfVxuICBzZXQgZGV2aWNlc0xpbmsgKGRldmljZXNMaW5rOiBzdHJpbmcpIHsgdGhpcy5fZGV2aWNlc0xpbmsgPSBkZXZpY2VzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9hY2N0U2V0dGluZ3NMaW5rOiBzdHJpbmc7XG4gIGdldCBhY2N0U2V0dGluZ3NMaW5rICgpIHsgcmV0dXJuIHRoaXMuX2FjY3RTZXR0aW5nc0xpbms7IH1cbiAgc2V0IGFjY3RTZXR0aW5nc0xpbmsgKGFjY3RTZXR0aW5nc0xpbms6IHN0cmluZykgeyB0aGlzLl9hY2N0U2V0dGluZ3NMaW5rID0gYWNjdFNldHRpbmdzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9zd2l0Y2hPcmdMaW5rOiBzdHJpbmc7XG4gIGdldCBzd2l0Y2hPcmdMaW5rICgpIHsgcmV0dXJuIHRoaXMuX3N3aXRjaE9yZ0xpbms7IH1cbiAgc2V0IHN3aXRjaE9yZ0xpbmsgKHN3aXRjaE9yZ0xpbms6IHN0cmluZykgeyB0aGlzLl9zd2l0Y2hPcmdMaW5rID0gc3dpdGNoT3JnTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF91c2VyU2V0dGluZ3NMaW5rOiBzdHJpbmc7XG4gIGdldCB1c2VyU2V0dGluZ3NMaW5rICgpIHsgcmV0dXJuIHRoaXMuX3VzZXJTZXR0aW5nc0xpbms7IH1cbiAgc2V0IHVzZXJTZXR0aW5nc0xpbmsgKHVzZXJTZXR0aW5nc0xpbms6IHN0cmluZykgeyB0aGlzLl91c2VyU2V0dGluZ3NMaW5rID0gdXNlclNldHRpbmdzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9zaWdub3V0TGluazogc3RyaW5nO1xuICBnZXQgc2lnbm91dExpbmsgKCkgeyByZXR1cm4gdGhpcy5fc2lnbm91dExpbms7IH1cbiAgc2V0IHNpZ25vdXRMaW5rIChzaWdub3V0TGluazogc3RyaW5nKSB7IHRoaXMuX3NpZ25vdXRMaW5rID0gc2lnbm91dExpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfbG9naW5MaW5rOiBzdHJpbmc7XG4gIGdldCBsb2dpbkxpbmsgKCkgeyByZXR1cm4gdGhpcy5fbG9naW5MaW5rOyB9XG4gIHNldCBsb2dpbkxpbmsgKGxvZ2luTGluazogc3RyaW5nKSB7IHRoaXMuX2xvZ2luTGluayA9IGxvZ2luTGluazsgfVxuXG4gIC8vIEtleSB0byBpZGVudGlmeSB0b2tlbiBpbiBjb29raWUgLy9cbiAgcHJpdmF0ZSBfYXV0aEtleTogc3RyaW5nO1xuICBnZXQgYXV0aEtleSAoKSB7IHJldHVybiB0aGlzLl9hdXRoS2V5OyB9XG4gIHNldCBhdXRoS2V5IChhdXRoS2V5OiBzdHJpbmcpIHsgdGhpcy5fYXV0aEtleSA9IGF1dGhLZXk7IH1cblxuICAvLyBLZXkgdG8gaWRlbnRpZnkgdG9rZW4gaW4gY29va2llIC8vXG4gIHByaXZhdGUgX3VzZXI6IFJldHJpZXZlclVzZXI7XG4gIGdldCB1c2VyICgpIHsgcmV0dXJuIHRoaXMuX3VzZXI7IH1cbiAgc2V0IHVzZXIgKHVzZXI6IFJldHJpZXZlclVzZXIpIHsgdGhpcy5fdXNlciA9IHVzZXI7IH1cblxuICBwcml2YXRlIGh0dHBPcHRpb25zID0ge1xuICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQ29udGVudC1UeXBlJzogICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICdBdXRob3JpemF0aW9uJzogJ0JlYXJlciAnICsgdGhpcy5fY29va2llU2VydmljZS5nZXQoJ3Rva2VuJylcbiAgICB9KVxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCwgcHJpdmF0ZSBfY29va2llU2VydmljZTogQ29va2llU2VydmljZSkgeyB9XG5cbiAgdmVyaWZ5VXNlciAoKSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFxuICAgICAgLmdldCh0aGlzLmF1dGhIb3N0ICsgJ3ZlcmlmeScsIHRoaXMuaHR0cE9wdGlvbnMpO1xuICB9XG59XG4iXX0=