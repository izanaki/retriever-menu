import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { Injectable, NgModule, Component, Input, ViewEncapsulation, defineInjectable, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var RetrieverMenuService = /** @class */ (function () {
    function RetrieverMenuService(http, _cookieService) {
        this.http = http;
        this._cookieService = _cookieService;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._cookieService.get('token')
            })
        };
    }
    Object.defineProperty(RetrieverMenuService.prototype, "authHost", {
        get: /**
         * @return {?}
         */
        function () { return this._authHost; },
        set: /**
         * @param {?} authHost
         * @return {?}
         */
        function (authHost) { this._authHost = authHost; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "playlistsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._playlistsLink; },
        set: /**
         * @param {?} playlistsLink
         * @return {?}
         */
        function (playlistsLink) { this._playlistsLink = playlistsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "templatesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._templatesLink; },
        set: /**
         * @param {?} templatesLink
         * @return {?}
         */
        function (templatesLink) { this._templatesLink = templatesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "savedTemplatesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._savedTemplatesLink; },
        set: /**
         * @param {?} savedTemplatesLink
         * @return {?}
         */
        function (savedTemplatesLink) { this._savedTemplatesLink = savedTemplatesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "mediaLibLink", {
        get: /**
         * @return {?}
         */
        function () { return this._mediaLibLink; },
        set: /**
         * @param {?} mediaLibLink
         * @return {?}
         */
        function (mediaLibLink) { this._mediaLibLink = mediaLibLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "appsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._appsLink; },
        set: /**
         * @param {?} appsLink
         * @return {?}
         */
        function (appsLink) { this._appsLink = appsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "devicesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._devicesLink; },
        set: /**
         * @param {?} devicesLink
         * @return {?}
         */
        function (devicesLink) { this._devicesLink = devicesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "acctSettingsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._acctSettingsLink; },
        set: /**
         * @param {?} acctSettingsLink
         * @return {?}
         */
        function (acctSettingsLink) { this._acctSettingsLink = acctSettingsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "switchOrgLink", {
        get: /**
         * @return {?}
         */
        function () { return this._switchOrgLink; },
        set: /**
         * @param {?} switchOrgLink
         * @return {?}
         */
        function (switchOrgLink) { this._switchOrgLink = switchOrgLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "userSettingsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._userSettingsLink; },
        set: /**
         * @param {?} userSettingsLink
         * @return {?}
         */
        function (userSettingsLink) { this._userSettingsLink = userSettingsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "signoutLink", {
        get: /**
         * @return {?}
         */
        function () { return this._signoutLink; },
        set: /**
         * @param {?} signoutLink
         * @return {?}
         */
        function (signoutLink) { this._signoutLink = signoutLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "loginLink", {
        get: /**
         * @return {?}
         */
        function () { return this._loginLink; },
        set: /**
         * @param {?} loginLink
         * @return {?}
         */
        function (loginLink) { this._loginLink = loginLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "authKey", {
        get: /**
         * @return {?}
         */
        function () { return this._authKey; },
        set: /**
         * @param {?} authKey
         * @return {?}
         */
        function (authKey) { this._authKey = authKey; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "user", {
        get: /**
         * @return {?}
         */
        function () { return this._user; },
        set: /**
         * @param {?} user
         * @return {?}
         */
        function (user) { this._user = user; },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    RetrieverMenuService.prototype.verifyUser = /**
     * @return {?}
     */
    function () {
        return this.http
            .get(this.authHost + 'verify', this.httpOptions);
    };
    RetrieverMenuService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    RetrieverMenuService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: CookieService }
    ]; };
    /** @nocollapse */ RetrieverMenuService.ngInjectableDef = defineInjectable({ factory: function RetrieverMenuService_Factory() { return new RetrieverMenuService(inject(HttpClient), inject(CookieService)); }, token: RetrieverMenuService, providedIn: "root" });
    return RetrieverMenuService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var RetrieverMenuComponent = /** @class */ (function () {
    function RetrieverMenuComponent(_menuService, router) {
        this._menuService = _menuService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    RetrieverMenuComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.tokenKey = this.tokenKey ? this.tokenKey : 'token';
        this.playlistsLink = this.playlistsLink ? this.playlistsLink : '';
        this.templatesLink = this.templatesLink ? this.templatesLink : '';
        this.savedTemplatesLink = this.savedTemplatesLink ? this.savedTemplatesLink : '';
        this.mediaLibLink = this.mediaLibLink ? this.mediaLibLink : '';
        this.devicesLink = this.devicesLink ? this.devicesLink : '';
        this.acctSettingsLink = this.acctSettingsLink ? this.acctSettingsLink : '';
        this.switchOrgLink = this.switchOrgLink ? this.switchOrgLink : '';
        this.userSettingsLink = this.userSettingsLink ? this.userSettingsLink : '';
        this.signoutLink = this.signoutLink ? this.signoutLink : '';
        this.loginLink = this.loginLink ? this.loginLink : '';
        this._menuService.authHost = this.authHost ? this.authHost : '';
        if (this.user && this.user.organization) {
            this._menuService.user = this.user;
        }
        else {
            this._menuService.verifyUser().subscribe(function (result) {
                if (result.user && result.user.organization) {
                    _this._menuService.user = result.user;
                }
                else {
                    window.location.href = _this._menuService.loginLink;
                }
            });
        }
    };
    /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    RetrieverMenuComponent.prototype.route = /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    function (e, link) {
        e.preventDefault();
        console.log('link', link);
        if (link.substr(0, 1) === '/') {
            /** @type {?} */
            var word = link.substr(1);
            if (!this.router.config || (this.router.config && !this.router.config.length)) {
                console.log('no config');
                window.location.href = link;
            }
            for (var a = 0; a !== this.router.config.length; a++) {
                console.log(this.router.config[a].path, word);
                if (this.router.config[a].path === word) {
                    console.log('moving to route');
                    this.router.navigate([word]);
                    return true;
                }
                if (this.router.config[a].children) {
                    for (var b = 0; b !== this.router.config[a].children.length; a++) {
                        if (this.router.config[a].children[b].path === word || this.router.config[a].children[b].redirectTo === word) {
                            console.log('moving to child route');
                            this.router.navigate([word]);
                            return true;
                        }
                    }
                }
            }
            console.log('none');
            window.location.href = link;
        }
        else {
            console.log('no slash ');
            window.location.href = link;
        }
    };
    RetrieverMenuComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-retriever-menu',
                    template: "<head>\n\t<meta charset=\"UTF-8\">\n\t<title>Playlist Manager</title>\n\t<link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500,900\" rel=\"stylesheet\">\n\t<link href=\"https://fonts.googleapis.com/css?family=Jolly+Lodger|Fontdiner+Swanky|Spicy+Rice\" rel=\"stylesheet\">\n</head>\n\n<!-- Site Content -->\n<header class=\"site-header\">\n\t<nav class=\"navbar fixed-top navbar-expand navbar-dark bg-dark\">\n\t\t<a class=\"navbar-brand mr-auto\" [href]=\"loginLink\" (click)=\"route($event, loginLink)\">Retriever</a>\n\t\t<a class=\"nav-link\" href=\"#\"><i class=\"fas fa-question-circle\"></i> Help</a>\n\t\t<!--<a class=\"nav-link dropup-toggle\" href=\"#\">Account</a>-->\n\t\t<input type=\"checkbox\" id=\"nav-account\" class=\"nav-check\">\n\t\t<label for=\"nav-account\" class=\"dropup-toggle\" *ngIf=\"_menuService.user\">\n\t\t\t<span class=\"sr-only\">Menu</span>\n\t\t\t<span class=\"menu-icon\">\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--top\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--middle\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--bottom\"></span>\n\t\t\t\t</span>\n\t\t</label>\n\t\t<div class=\"dropup-menu\" *ngIf=\"_menuService.user\">\n\t\t\t<div class=\"dropup-menu__content\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"playlistsLink\" (click)=\"route($event, playlistsLink)\" class=\"dropup-item\"><i class=\"fas fa-th-list\"></i> Playlists</a>\n\t\t\t\t\t\t\t\t<a [href]=\"templatesLink\" (click)=\"route($event, templatesLink)\" class=\"dropup-item\"><i class=\"fas fa-th-large\"></i> Templates</a>\n\t\t\t\t\t\t\t\t<a [href]=\"savedTemplatesLink\" (click)=\"route($event, savedTemplatesLink)\" class=\"dropup-item\"><i class=\"fas fa-star\"></i> Saved Templates</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"mediaLibLink\" (click)=\"route($event, mediaLibLink)\" class=\"dropup-item\"><i class=\"far fa-images\"></i> Media Library</a>\n\t\t\t\t\t\t\t\t<a [href]=\"appsLink\" (click)=\"route($event, appsLink)\" class=\"dropup-item\"><i class=\"fas fa-th\"></i> Apps</a>\n\t\t\t\t\t\t\t\t<a [href]=\"devicesLink\" (click)=\"route($event, devicesLink)\" class=\"dropup-item\"><i class=\"fas fa-server\"></i> Devices</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm border-left-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-sm-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">{{_menuService.user.organization.name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"acctSettingsLink\" (click)=\"route($event, acctSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> Account Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"switchOrgLink\" (click)=\"route($event, switchOrgLink)\"><i class=\"fas fa-exchange-alt\"></i> Switch Organization</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-lg-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">\n\t\t\t\t\t\t\t\t\t<!--<img class=\"avatar\" src=\"images/DTS_005-FACES_CIRCLE_L.png\" alt=\"\"> -->\n\t\t\t\t\t\t\t\t\t{{_menuService.user.first_name}} {{_menuService.user.last_name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"userSettingsLink\" (click)=\"route($event, userSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> User Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"signoutLink\" (click)=\"route($event, signoutLink)\"><i class=\"fas fa-sign-out-alt\"></i> Sign Out</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div><!-- -->\n\t\t\t</div>\n\t\t</div>\n\t</nav>\n</header>\n",
                    encapsulation: ViewEncapsulation.None
                }] }
    ];
    /** @nocollapse */
    RetrieverMenuComponent.ctorParameters = function () { return [
        { type: RetrieverMenuService },
        { type: Router }
    ]; };
    RetrieverMenuComponent.propDecorators = {
        user: [{ type: Input }],
        tokenKey: [{ type: Input }],
        authHost: [{ type: Input }],
        playlistsLink: [{ type: Input }],
        templatesLink: [{ type: Input }],
        savedTemplatesLink: [{ type: Input }],
        mediaLibLink: [{ type: Input }],
        appsLink: [{ type: Input }],
        devicesLink: [{ type: Input }],
        acctSettingsLink: [{ type: Input }],
        switchOrgLink: [{ type: Input }],
        userSettingsLink: [{ type: Input }],
        signoutLink: [{ type: Input }],
        loginLink: [{ type: Input }]
    };
    return RetrieverMenuComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var RetrieverMenuModule = /** @class */ (function () {
    function RetrieverMenuModule() {
    }
    RetrieverMenuModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [RetrieverMenuComponent],
                    imports: [CommonModule, RouterModule.forRoot([])],
                    exports: [RetrieverMenuComponent]
                },] }
    ];
    return RetrieverMenuModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { RetrieverMenuService, RetrieverMenuComponent, RetrieverMenuModule };

//# sourceMappingURL=retriever-menu.js.map