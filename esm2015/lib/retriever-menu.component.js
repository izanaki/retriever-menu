/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { RetrieverMenuService } from './retriever-menu.service';
import { Router } from '@angular/router';
export class RetrieverMenuComponent {
    /**
     * @param {?} _menuService
     * @param {?} router
     */
    constructor(_menuService, router) {
        this._menuService = _menuService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.tokenKey = this.tokenKey ? this.tokenKey : 'token';
        this.playlistsLink = this.playlistsLink ? this.playlistsLink : '';
        this.templatesLink = this.templatesLink ? this.templatesLink : '';
        this.savedTemplatesLink = this.savedTemplatesLink ? this.savedTemplatesLink : '';
        this.mediaLibLink = this.mediaLibLink ? this.mediaLibLink : '';
        this.devicesLink = this.devicesLink ? this.devicesLink : '';
        this.acctSettingsLink = this.acctSettingsLink ? this.acctSettingsLink : '';
        this.switchOrgLink = this.switchOrgLink ? this.switchOrgLink : '';
        this.userSettingsLink = this.userSettingsLink ? this.userSettingsLink : '';
        this.signoutLink = this.signoutLink ? this.signoutLink : '';
        this.loginLink = this.loginLink ? this.loginLink : '';
        this._menuService.authHost = this.authHost ? this.authHost : '';
        if (this.user && this.user.organization) {
            this._menuService.user = this.user;
        }
        else {
            this._menuService.verifyUser().subscribe((result) => {
                if (result.user && result.user.organization) {
                    this._menuService.user = result.user;
                }
                else {
                    window.location.href = this._menuService.loginLink;
                }
            });
        }
    }
    /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    route(e, link) {
        e.preventDefault();
        console.log('link', link);
        if (link.substr(0, 1) === '/') {
            /** @type {?} */
            let word = link.substr(1);
            if (!this.router.config || (this.router.config && !this.router.config.length)) {
                console.log('no config');
                window.location.href = link;
            }
            for (let a = 0; a !== this.router.config.length; a++) {
                console.log(this.router.config[a].path, word);
                if (this.router.config[a].path === word) {
                    console.log('moving to route');
                    this.router.navigate([word]);
                    return true;
                }
                if (this.router.config[a].children) {
                    for (let b = 0; b !== this.router.config[a].children.length; a++) {
                        if (this.router.config[a].children[b].path === word || this.router.config[a].children[b].redirectTo === word) {
                            console.log('moving to child route');
                            this.router.navigate([word]);
                            return true;
                        }
                    }
                }
            }
            console.log('none');
            window.location.href = link;
        }
        else {
            console.log('no slash ');
            window.location.href = link;
        }
    }
}
RetrieverMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-retriever-menu',
                template: "<head>\n\t<meta charset=\"UTF-8\">\n\t<title>Playlist Manager</title>\n\t<link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500,900\" rel=\"stylesheet\">\n\t<link href=\"https://fonts.googleapis.com/css?family=Jolly+Lodger|Fontdiner+Swanky|Spicy+Rice\" rel=\"stylesheet\">\n</head>\n\n<!-- Site Content -->\n<header class=\"site-header\">\n\t<nav class=\"navbar fixed-top navbar-expand navbar-dark bg-dark\">\n\t\t<a class=\"navbar-brand mr-auto\" [href]=\"loginLink\" (click)=\"route($event, loginLink)\">Retriever</a>\n\t\t<a class=\"nav-link\" href=\"#\"><i class=\"fas fa-question-circle\"></i> Help</a>\n\t\t<!--<a class=\"nav-link dropup-toggle\" href=\"#\">Account</a>-->\n\t\t<input type=\"checkbox\" id=\"nav-account\" class=\"nav-check\">\n\t\t<label for=\"nav-account\" class=\"dropup-toggle\" *ngIf=\"_menuService.user\">\n\t\t\t<span class=\"sr-only\">Menu</span>\n\t\t\t<span class=\"menu-icon\">\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--top\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--middle\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--bottom\"></span>\n\t\t\t\t</span>\n\t\t</label>\n\t\t<div class=\"dropup-menu\" *ngIf=\"_menuService.user\">\n\t\t\t<div class=\"dropup-menu__content\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"playlistsLink\" (click)=\"route($event, playlistsLink)\" class=\"dropup-item\"><i class=\"fas fa-th-list\"></i> Playlists</a>\n\t\t\t\t\t\t\t\t<a [href]=\"templatesLink\" (click)=\"route($event, templatesLink)\" class=\"dropup-item\"><i class=\"fas fa-th-large\"></i> Templates</a>\n\t\t\t\t\t\t\t\t<a [href]=\"savedTemplatesLink\" (click)=\"route($event, savedTemplatesLink)\" class=\"dropup-item\"><i class=\"fas fa-star\"></i> Saved Templates</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"mediaLibLink\" (click)=\"route($event, mediaLibLink)\" class=\"dropup-item\"><i class=\"far fa-images\"></i> Media Library</a>\n\t\t\t\t\t\t\t\t<a [href]=\"appsLink\" (click)=\"route($event, appsLink)\" class=\"dropup-item\"><i class=\"fas fa-th\"></i> Apps</a>\n\t\t\t\t\t\t\t\t<a [href]=\"devicesLink\" (click)=\"route($event, devicesLink)\" class=\"dropup-item\"><i class=\"fas fa-server\"></i> Devices</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm border-left-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-sm-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">{{_menuService.user.organization.name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"acctSettingsLink\" (click)=\"route($event, acctSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> Account Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"switchOrgLink\" (click)=\"route($event, switchOrgLink)\"><i class=\"fas fa-exchange-alt\"></i> Switch Organization</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-lg-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">\n\t\t\t\t\t\t\t\t\t<!--<img class=\"avatar\" src=\"images/DTS_005-FACES_CIRCLE_L.png\" alt=\"\"> -->\n\t\t\t\t\t\t\t\t\t{{_menuService.user.first_name}} {{_menuService.user.last_name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"userSettingsLink\" (click)=\"route($event, userSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> User Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"signoutLink\" (click)=\"route($event, signoutLink)\"><i class=\"fas fa-sign-out-alt\"></i> Sign Out</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div><!-- -->\n\t\t\t</div>\n\t\t</div>\n\t</nav>\n</header>\n",
                encapsulation: ViewEncapsulation.None
            }] }
];
/** @nocollapse */
RetrieverMenuComponent.ctorParameters = () => [
    { type: RetrieverMenuService },
    { type: Router }
];
RetrieverMenuComponent.propDecorators = {
    user: [{ type: Input }],
    tokenKey: [{ type: Input }],
    authHost: [{ type: Input }],
    playlistsLink: [{ type: Input }],
    templatesLink: [{ type: Input }],
    savedTemplatesLink: [{ type: Input }],
    mediaLibLink: [{ type: Input }],
    appsLink: [{ type: Input }],
    devicesLink: [{ type: Input }],
    acctSettingsLink: [{ type: Input }],
    switchOrgLink: [{ type: Input }],
    userSettingsLink: [{ type: Input }],
    signoutLink: [{ type: Input }],
    loginLink: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    RetrieverMenuComponent.prototype.user;
    /** @type {?} */
    RetrieverMenuComponent.prototype.tokenKey;
    /** @type {?} */
    RetrieverMenuComponent.prototype.authHost;
    /** @type {?} */
    RetrieverMenuComponent.prototype.playlistsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.templatesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.savedTemplatesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.mediaLibLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.appsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.devicesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.acctSettingsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.switchOrgLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.userSettingsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.signoutLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.loginLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.isLogged;
    /** @type {?} */
    RetrieverMenuComponent.prototype._menuService;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcmV0cmlldmVyLW1lbnUvIiwic291cmNlcyI6WyJsaWIvcmV0cmlldmVyLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBVSxpQkFBaUIsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMxRSxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUc5RCxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFRdkMsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUFrQmpDLFlBQW1CLFlBQWtDLEVBQVUsTUFBYztRQUExRCxpQkFBWSxHQUFaLFlBQVksQ0FBc0I7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQzdFLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDeEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDakYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDL0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFdEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRWhFLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3BDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQWtCLEVBQUUsRUFBRTtnQkFDOUQsSUFBSSxNQUFNLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO29CQUMzQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2lCQUN0QztxQkFBTTtvQkFDTCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztpQkFDcEQ7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7O0lBRUQsS0FBSyxDQUFFLENBQU0sRUFBRSxJQUFZO1FBQ3pCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUUxQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTs7Z0JBQ3hCLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFHO2dCQUM5RSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDN0I7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO29CQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDN0IsT0FBTyxJQUFJLENBQUM7aUJBQ2I7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7b0JBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUNoRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFOzRCQUM1RyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7NEJBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsT0FBTyxJQUFJLENBQUM7eUJBQ2I7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBQzdCO2FBQU07WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztTQUM3QjtJQUNILENBQUM7OztZQTFGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsODBIQUFrQztnQkFFbEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDdEM7Ozs7WUFWTyxvQkFBb0I7WUFHcEIsTUFBTTs7O21CQVNYLEtBQUs7dUJBQ0wsS0FBSzt1QkFDTCxLQUFLOzRCQUNMLEtBQUs7NEJBQ0wsS0FBSztpQ0FDTCxLQUFLOzJCQUNMLEtBQUs7dUJBQ0wsS0FBSzswQkFDTCxLQUFLOytCQUNMLEtBQUs7NEJBQ0wsS0FBSzsrQkFDTCxLQUFLOzBCQUNMLEtBQUs7d0JBQ0wsS0FBSzs7OztJQWJOLHNDQUE2Qjs7SUFDN0IsMENBQTBCOztJQUMxQiwwQ0FBMEI7O0lBQzFCLCtDQUErQjs7SUFDL0IsK0NBQStCOztJQUMvQixvREFBb0M7O0lBQ3BDLDhDQUE4Qjs7SUFDOUIsMENBQTBCOztJQUMxQiw2Q0FBNkI7O0lBQzdCLGtEQUFrQzs7SUFDbEMsK0NBQStCOztJQUMvQixrREFBa0M7O0lBQ2xDLDZDQUE2Qjs7SUFDN0IsMkNBQTJCOztJQUUzQiwwQ0FBYzs7SUFFRiw4Q0FBeUM7Ozs7O0lBQUUsd0NBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9ufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7UmV0cmlldmVyTWVudVNlcnZpY2V9IGZyb20gJy4vcmV0cmlldmVyLW1lbnUuc2VydmljZSc7XG5pbXBvcnQge0F1dGhyZXN1bHR9IGZyb20gJy4vaW50ZXJmYWNlcy9hdXRocmVzdWx0JztcbmltcG9ydCB7UmV0cmlldmVyVXNlcn0gZnJvbSAnLi9pbnRlcmZhY2VzL3VzZXInO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1yZXRyaWV2ZXItbWVudScsXG4gIHRlbXBsYXRlVXJsOiAncmV0cmlldmVyLW1lbnUuaHRtbCcsXG4gIHN0eWxlVXJsczogWyAgXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBSZXRyaWV2ZXJNZW51Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdXNlcjogUmV0cmlldmVyVXNlcjtcbiAgQElucHV0KCkgdG9rZW5LZXk6IHN0cmluZztcbiAgQElucHV0KCkgYXV0aEhvc3Q6IHN0cmluZztcbiAgQElucHV0KCkgcGxheWxpc3RzTGluazogc3RyaW5nO1xuICBASW5wdXQoKSB0ZW1wbGF0ZXNMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHNhdmVkVGVtcGxhdGVzTGluazogc3RyaW5nO1xuICBASW5wdXQoKSBtZWRpYUxpYkxpbms6IHN0cmluZztcbiAgQElucHV0KCkgYXBwc0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgZGV2aWNlc0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgYWNjdFNldHRpbmdzTGluazogc3RyaW5nO1xuICBASW5wdXQoKSBzd2l0Y2hPcmdMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHVzZXJTZXR0aW5nc0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgc2lnbm91dExpbms6IHN0cmluZztcbiAgQElucHV0KCkgbG9naW5MaW5rOiBzdHJpbmc7XG5cbiAgaXNMb2dnZWQ6IGFueTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgX21lbnVTZXJ2aWNlOiBSZXRyaWV2ZXJNZW51U2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy50b2tlbktleSA9IHRoaXMudG9rZW5LZXkgPyB0aGlzLnRva2VuS2V5IDogJ3Rva2VuJztcbiAgICB0aGlzLnBsYXlsaXN0c0xpbmsgPSB0aGlzLnBsYXlsaXN0c0xpbmsgPyB0aGlzLnBsYXlsaXN0c0xpbmsgOiAnJztcbiAgICB0aGlzLnRlbXBsYXRlc0xpbmsgPSB0aGlzLnRlbXBsYXRlc0xpbmsgPyB0aGlzLnRlbXBsYXRlc0xpbmsgOiAnJztcbiAgICB0aGlzLnNhdmVkVGVtcGxhdGVzTGluayA9IHRoaXMuc2F2ZWRUZW1wbGF0ZXNMaW5rID8gdGhpcy5zYXZlZFRlbXBsYXRlc0xpbmsgOiAnJztcbiAgICB0aGlzLm1lZGlhTGliTGluayA9IHRoaXMubWVkaWFMaWJMaW5rID8gdGhpcy5tZWRpYUxpYkxpbmsgOiAnJztcbiAgICB0aGlzLmRldmljZXNMaW5rID0gdGhpcy5kZXZpY2VzTGluayA/IHRoaXMuZGV2aWNlc0xpbmsgOiAnJztcbiAgICB0aGlzLmFjY3RTZXR0aW5nc0xpbmsgPSB0aGlzLmFjY3RTZXR0aW5nc0xpbmsgPyB0aGlzLmFjY3RTZXR0aW5nc0xpbmsgOiAnJztcbiAgICB0aGlzLnN3aXRjaE9yZ0xpbmsgPSB0aGlzLnN3aXRjaE9yZ0xpbmsgPyB0aGlzLnN3aXRjaE9yZ0xpbmsgOiAnJztcbiAgICB0aGlzLnVzZXJTZXR0aW5nc0xpbmsgPSB0aGlzLnVzZXJTZXR0aW5nc0xpbmsgPyB0aGlzLnVzZXJTZXR0aW5nc0xpbmsgOiAnJztcbiAgICB0aGlzLnNpZ25vdXRMaW5rID0gdGhpcy5zaWdub3V0TGluayA/IHRoaXMuc2lnbm91dExpbmsgOiAnJztcbiAgICB0aGlzLmxvZ2luTGluayA9IHRoaXMubG9naW5MaW5rID8gdGhpcy5sb2dpbkxpbmsgOiAnJztcblxuICAgIHRoaXMuX21lbnVTZXJ2aWNlLmF1dGhIb3N0ID0gdGhpcy5hdXRoSG9zdCA/IHRoaXMuYXV0aEhvc3QgOiAnJztcblxuICAgIGlmICh0aGlzLnVzZXIgJiYgdGhpcy51c2VyLm9yZ2FuaXphdGlvbikge1xuICAgICAgdGhpcy5fbWVudVNlcnZpY2UudXNlciA9IHRoaXMudXNlcjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fbWVudVNlcnZpY2UudmVyaWZ5VXNlcigpLnN1YnNjcmliZSgocmVzdWx0OiBBdXRocmVzdWx0KSA9PiB7XG4gICAgICAgIGlmIChyZXN1bHQudXNlciAmJiByZXN1bHQudXNlci5vcmdhbml6YXRpb24pIHtcbiAgICAgICAgICB0aGlzLl9tZW51U2VydmljZS51c2VyID0gcmVzdWx0LnVzZXI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLl9tZW51U2VydmljZS5sb2dpbkxpbms7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHJvdXRlIChlOiBhbnksIGxpbms6IHN0cmluZykge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgIGNvbnNvbGUubG9nKCdsaW5rJywgbGluayk7XG5cbiAgICBpZiAobGluay5zdWJzdHIoMCwxKSA9PT0gJy8nKSB7XG4gICAgICBsZXQgd29yZCA9IGxpbmsuc3Vic3RyKDEpO1xuICAgICAgaWYgKCF0aGlzLnJvdXRlci5jb25maWcgfHwgKHRoaXMucm91dGVyLmNvbmZpZyAmJiAhdGhpcy5yb3V0ZXIuY29uZmlnLmxlbmd0aCkgKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdubyBjb25maWcnKTtcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBsaW5rO1xuICAgICAgfVxuXG4gICAgICBmb3IgKGxldCBhID0gMDsgYSAhPT0gdGhpcy5yb3V0ZXIuY29uZmlnLmxlbmd0aDsgYSsrKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMucm91dGVyLmNvbmZpZ1thXS5wYXRoLCB3b3JkKTtcbiAgICAgICAgaWYgKHRoaXMucm91dGVyLmNvbmZpZ1thXS5wYXRoID09PSB3b3JkKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ21vdmluZyB0byByb3V0ZScpO1xuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt3b3JkXSk7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucm91dGVyLmNvbmZpZ1thXS5jaGlsZHJlbikge1xuICAgICAgICAgIGZvciAobGV0IGIgPSAwOyBiICE9PSB0aGlzLnJvdXRlci5jb25maWdbYV0uY2hpbGRyZW4ubGVuZ3RoOyBhKyspIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJvdXRlci5jb25maWdbYV0uY2hpbGRyZW5bYl0ucGF0aCA9PT0gd29yZCB8fCB0aGlzLnJvdXRlci5jb25maWdbYV0uY2hpbGRyZW5bYl0ucmVkaXJlY3RUbyA9PT0gd29yZCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbW92aW5nIHRvIGNoaWxkIHJvdXRlJyk7XG4gICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt3b3JkXSk7XG4gICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgY29uc29sZS5sb2coJ25vbmUnKTtcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gbGluaztcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS5sb2coJ25vIHNsYXNoICcpO1xuICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBsaW5rO1xuICAgIH1cbiAgfVxufVxuIl19