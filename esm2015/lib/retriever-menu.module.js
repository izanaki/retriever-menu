/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RetrieverMenuComponent } from './retriever-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
export class RetrieverMenuModule {
}
RetrieverMenuModule.decorators = [
    { type: NgModule, args: [{
                declarations: [RetrieverMenuComponent],
                imports: [CommonModule, RouterModule.forRoot([])],
                exports: [RetrieverMenuComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcmV0cmlldmVyLW1lbnUvIiwic291cmNlcyI6WyJsaWIvcmV0cmlldmVyLW1lbnUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFPN0MsTUFBTSxPQUFPLG1CQUFtQjs7O1lBTC9CLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztnQkFDdEMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUU7Z0JBQ25ELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2FBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJldHJpZXZlck1lbnVDb21wb25lbnQgfSBmcm9tICcuL3JldHJpZXZlci1tZW51LmNvbXBvbmVudCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtSZXRyaWV2ZXJNZW51Q29tcG9uZW50XSxcbiAgaW1wb3J0czogWyBDb21tb25Nb2R1bGUsIFJvdXRlck1vZHVsZS5mb3JSb290KFtdKSBdLFxuICBleHBvcnRzOiBbUmV0cmlldmVyTWVudUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgUmV0cmlldmVyTWVudU1vZHVsZSB7IH1cbiJdfQ==