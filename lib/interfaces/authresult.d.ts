import { RetrieverUser } from './user';
export interface Authresult {
    status: string;
    token: string;
    user: RetrieverUser;
}
