/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { RetrieverMenuService } from './retriever-menu.service';
import { Router } from '@angular/router';
var RetrieverMenuComponent = /** @class */ (function () {
    function RetrieverMenuComponent(_menuService, router) {
        this._menuService = _menuService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    RetrieverMenuComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.tokenKey = this.tokenKey ? this.tokenKey : 'token';
        this.playlistsLink = this.playlistsLink ? this.playlistsLink : '';
        this.templatesLink = this.templatesLink ? this.templatesLink : '';
        this.savedTemplatesLink = this.savedTemplatesLink ? this.savedTemplatesLink : '';
        this.mediaLibLink = this.mediaLibLink ? this.mediaLibLink : '';
        this.devicesLink = this.devicesLink ? this.devicesLink : '';
        this.acctSettingsLink = this.acctSettingsLink ? this.acctSettingsLink : '';
        this.switchOrgLink = this.switchOrgLink ? this.switchOrgLink : '';
        this.userSettingsLink = this.userSettingsLink ? this.userSettingsLink : '';
        this.signoutLink = this.signoutLink ? this.signoutLink : '';
        this.loginLink = this.loginLink ? this.loginLink : '';
        this._menuService.authHost = this.authHost ? this.authHost : '';
        if (this.user && this.user.organization) {
            this._menuService.user = this.user;
        }
        else {
            this._menuService.verifyUser().subscribe(function (result) {
                if (result.user && result.user.organization) {
                    _this._menuService.user = result.user;
                }
                else {
                    window.location.href = _this._menuService.loginLink;
                }
            });
        }
    };
    /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    RetrieverMenuComponent.prototype.route = /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    function (e, link) {
        e.preventDefault();
        console.log('link', link);
        if (link.substr(0, 1) === '/') {
            /** @type {?} */
            var word = link.substr(1);
            if (!this.router.config || (this.router.config && !this.router.config.length)) {
                console.log('no config');
                window.location.href = link;
            }
            for (var a = 0; a !== this.router.config.length; a++) {
                console.log(this.router.config[a].path, word);
                if (this.router.config[a].path === word) {
                    console.log('moving to route');
                    this.router.navigate([word]);
                    return true;
                }
                if (this.router.config[a].children) {
                    for (var b = 0; b !== this.router.config[a].children.length; a++) {
                        if (this.router.config[a].children[b].path === word || this.router.config[a].children[b].redirectTo === word) {
                            console.log('moving to child route');
                            this.router.navigate([word]);
                            return true;
                        }
                    }
                }
            }
            console.log('none');
            window.location.href = link;
        }
        else {
            console.log('no slash ');
            window.location.href = link;
        }
    };
    RetrieverMenuComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-retriever-menu',
                    template: "<head>\n\t<meta charset=\"UTF-8\">\n\t<title>Playlist Manager</title>\n\t<link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500,900\" rel=\"stylesheet\">\n\t<link href=\"https://fonts.googleapis.com/css?family=Jolly+Lodger|Fontdiner+Swanky|Spicy+Rice\" rel=\"stylesheet\">\n</head>\n\n<!-- Site Content -->\n<header class=\"site-header\">\n\t<nav class=\"navbar fixed-top navbar-expand navbar-dark bg-dark\">\n\t\t<a class=\"navbar-brand mr-auto\" [href]=\"loginLink\" (click)=\"route($event, loginLink)\">Retriever</a>\n\t\t<a class=\"nav-link\" href=\"#\"><i class=\"fas fa-question-circle\"></i> Help</a>\n\t\t<!--<a class=\"nav-link dropup-toggle\" href=\"#\">Account</a>-->\n\t\t<input type=\"checkbox\" id=\"nav-account\" class=\"nav-check\">\n\t\t<label for=\"nav-account\" class=\"dropup-toggle\" *ngIf=\"_menuService.user\">\n\t\t\t<span class=\"sr-only\">Menu</span>\n\t\t\t<span class=\"menu-icon\">\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--top\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--middle\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--bottom\"></span>\n\t\t\t\t</span>\n\t\t</label>\n\t\t<div class=\"dropup-menu\" *ngIf=\"_menuService.user\">\n\t\t\t<div class=\"dropup-menu__content\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"playlistsLink\" (click)=\"route($event, playlistsLink)\" class=\"dropup-item\"><i class=\"fas fa-th-list\"></i> Playlists</a>\n\t\t\t\t\t\t\t\t<a [href]=\"templatesLink\" (click)=\"route($event, templatesLink)\" class=\"dropup-item\"><i class=\"fas fa-th-large\"></i> Templates</a>\n\t\t\t\t\t\t\t\t<a [href]=\"savedTemplatesLink\" (click)=\"route($event, savedTemplatesLink)\" class=\"dropup-item\"><i class=\"fas fa-star\"></i> Saved Templates</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"mediaLibLink\" (click)=\"route($event, mediaLibLink)\" class=\"dropup-item\"><i class=\"far fa-images\"></i> Media Library</a>\n\t\t\t\t\t\t\t\t<a [href]=\"appsLink\" (click)=\"route($event, appsLink)\" class=\"dropup-item\"><i class=\"fas fa-th\"></i> Apps</a>\n\t\t\t\t\t\t\t\t<a [href]=\"devicesLink\" (click)=\"route($event, devicesLink)\" class=\"dropup-item\"><i class=\"fas fa-server\"></i> Devices</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm border-left-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-sm-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">{{_menuService.user.organization.name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"acctSettingsLink\" (click)=\"route($event, acctSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> Account Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"switchOrgLink\" (click)=\"route($event, switchOrgLink)\"><i class=\"fas fa-exchange-alt\"></i> Switch Organization</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-lg-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">\n\t\t\t\t\t\t\t\t\t<!--<img class=\"avatar\" src=\"images/DTS_005-FACES_CIRCLE_L.png\" alt=\"\"> -->\n\t\t\t\t\t\t\t\t\t{{_menuService.user.first_name}} {{_menuService.user.last_name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"userSettingsLink\" (click)=\"route($event, userSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> User Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"signoutLink\" (click)=\"route($event, signoutLink)\"><i class=\"fas fa-sign-out-alt\"></i> Sign Out</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div><!-- -->\n\t\t\t</div>\n\t\t</div>\n\t</nav>\n</header>\n",
                    encapsulation: ViewEncapsulation.None
                }] }
    ];
    /** @nocollapse */
    RetrieverMenuComponent.ctorParameters = function () { return [
        { type: RetrieverMenuService },
        { type: Router }
    ]; };
    RetrieverMenuComponent.propDecorators = {
        user: [{ type: Input }],
        tokenKey: [{ type: Input }],
        authHost: [{ type: Input }],
        playlistsLink: [{ type: Input }],
        templatesLink: [{ type: Input }],
        savedTemplatesLink: [{ type: Input }],
        mediaLibLink: [{ type: Input }],
        appsLink: [{ type: Input }],
        devicesLink: [{ type: Input }],
        acctSettingsLink: [{ type: Input }],
        switchOrgLink: [{ type: Input }],
        userSettingsLink: [{ type: Input }],
        signoutLink: [{ type: Input }],
        loginLink: [{ type: Input }]
    };
    return RetrieverMenuComponent;
}());
export { RetrieverMenuComponent };
if (false) {
    /** @type {?} */
    RetrieverMenuComponent.prototype.user;
    /** @type {?} */
    RetrieverMenuComponent.prototype.tokenKey;
    /** @type {?} */
    RetrieverMenuComponent.prototype.authHost;
    /** @type {?} */
    RetrieverMenuComponent.prototype.playlistsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.templatesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.savedTemplatesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.mediaLibLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.appsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.devicesLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.acctSettingsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.switchOrgLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.userSettingsLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.signoutLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.loginLink;
    /** @type {?} */
    RetrieverMenuComponent.prototype.isLogged;
    /** @type {?} */
    RetrieverMenuComponent.prototype._menuService;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcmV0cmlldmVyLW1lbnUvIiwic291cmNlcyI6WyJsaWIvcmV0cmlldmVyLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBVSxpQkFBaUIsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMxRSxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUc5RCxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFdkM7SUF3QkUsZ0NBQW1CLFlBQWtDLEVBQVUsTUFBYztRQUExRCxpQkFBWSxHQUFaLFlBQVksQ0FBc0I7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQzdFLENBQUM7Ozs7SUFFRCx5Q0FBUTs7O0lBQVI7UUFBQSxpQkEwQkM7UUF6QkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDeEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDakYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDL0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFdEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRWhFLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3BDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLE1BQWtCO2dCQUMxRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQzNDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7aUJBQ3RDO3FCQUFNO29CQUNMLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2lCQUNwRDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7SUFFRCxzQ0FBSzs7Ozs7SUFBTCxVQUFPLENBQU0sRUFBRSxJQUFZO1FBQ3pCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUUxQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTs7Z0JBQ3hCLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFHO2dCQUM5RSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDN0I7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO29CQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDN0IsT0FBTyxJQUFJLENBQUM7aUJBQ2I7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7b0JBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUNoRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFOzRCQUM1RyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7NEJBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsT0FBTyxJQUFJLENBQUM7eUJBQ2I7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBQzdCO2FBQU07WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztTQUM3QjtJQUNILENBQUM7O2dCQTFGRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsODBIQUFrQztvQkFFbEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7aUJBQ3RDOzs7O2dCQVZPLG9CQUFvQjtnQkFHcEIsTUFBTTs7O3VCQVNYLEtBQUs7MkJBQ0wsS0FBSzsyQkFDTCxLQUFLO2dDQUNMLEtBQUs7Z0NBQ0wsS0FBSztxQ0FDTCxLQUFLOytCQUNMLEtBQUs7MkJBQ0wsS0FBSzs4QkFDTCxLQUFLO21DQUNMLEtBQUs7Z0NBQ0wsS0FBSzttQ0FDTCxLQUFLOzhCQUNMLEtBQUs7NEJBQ0wsS0FBSzs7SUF1RVIsNkJBQUM7Q0FBQSxBQTNGRCxJQTJGQztTQXJGWSxzQkFBc0I7OztJQUNqQyxzQ0FBNkI7O0lBQzdCLDBDQUEwQjs7SUFDMUIsMENBQTBCOztJQUMxQiwrQ0FBK0I7O0lBQy9CLCtDQUErQjs7SUFDL0Isb0RBQW9DOztJQUNwQyw4Q0FBOEI7O0lBQzlCLDBDQUEwQjs7SUFDMUIsNkNBQTZCOztJQUM3QixrREFBa0M7O0lBQ2xDLCtDQUErQjs7SUFDL0Isa0RBQWtDOztJQUNsQyw2Q0FBNkI7O0lBQzdCLDJDQUEyQjs7SUFFM0IsMENBQWM7O0lBRUYsOENBQXlDOzs7OztJQUFFLHdDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1JldHJpZXZlck1lbnVTZXJ2aWNlfSBmcm9tICcuL3JldHJpZXZlci1tZW51LnNlcnZpY2UnO1xuaW1wb3J0IHtBdXRocmVzdWx0fSBmcm9tICcuL2ludGVyZmFjZXMvYXV0aHJlc3VsdCc7XG5pbXBvcnQge1JldHJpZXZlclVzZXJ9IGZyb20gJy4vaW50ZXJmYWNlcy91c2VyJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItcmV0cmlldmVyLW1lbnUnLFxuICB0ZW1wbGF0ZVVybDogJ3JldHJpZXZlci1tZW51Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsgIF0sXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgUmV0cmlldmVyTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIHVzZXI6IFJldHJpZXZlclVzZXI7XG4gIEBJbnB1dCgpIHRva2VuS2V5OiBzdHJpbmc7XG4gIEBJbnB1dCgpIGF1dGhIb3N0OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHBsYXlsaXN0c0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgdGVtcGxhdGVzTGluazogc3RyaW5nO1xuICBASW5wdXQoKSBzYXZlZFRlbXBsYXRlc0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgbWVkaWFMaWJMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGFwcHNMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGRldmljZXNMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGFjY3RTZXR0aW5nc0xpbms6IHN0cmluZztcbiAgQElucHV0KCkgc3dpdGNoT3JnTGluazogc3RyaW5nO1xuICBASW5wdXQoKSB1c2VyU2V0dGluZ3NMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHNpZ25vdXRMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGxvZ2luTGluazogc3RyaW5nO1xuXG4gIGlzTG9nZ2VkOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHVibGljIF9tZW51U2VydmljZTogUmV0cmlldmVyTWVudVNlcnZpY2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMudG9rZW5LZXkgPSB0aGlzLnRva2VuS2V5ID8gdGhpcy50b2tlbktleSA6ICd0b2tlbic7XG4gICAgdGhpcy5wbGF5bGlzdHNMaW5rID0gdGhpcy5wbGF5bGlzdHNMaW5rID8gdGhpcy5wbGF5bGlzdHNMaW5rIDogJyc7XG4gICAgdGhpcy50ZW1wbGF0ZXNMaW5rID0gdGhpcy50ZW1wbGF0ZXNMaW5rID8gdGhpcy50ZW1wbGF0ZXNMaW5rIDogJyc7XG4gICAgdGhpcy5zYXZlZFRlbXBsYXRlc0xpbmsgPSB0aGlzLnNhdmVkVGVtcGxhdGVzTGluayA/IHRoaXMuc2F2ZWRUZW1wbGF0ZXNMaW5rIDogJyc7XG4gICAgdGhpcy5tZWRpYUxpYkxpbmsgPSB0aGlzLm1lZGlhTGliTGluayA/IHRoaXMubWVkaWFMaWJMaW5rIDogJyc7XG4gICAgdGhpcy5kZXZpY2VzTGluayA9IHRoaXMuZGV2aWNlc0xpbmsgPyB0aGlzLmRldmljZXNMaW5rIDogJyc7XG4gICAgdGhpcy5hY2N0U2V0dGluZ3NMaW5rID0gdGhpcy5hY2N0U2V0dGluZ3NMaW5rID8gdGhpcy5hY2N0U2V0dGluZ3NMaW5rIDogJyc7XG4gICAgdGhpcy5zd2l0Y2hPcmdMaW5rID0gdGhpcy5zd2l0Y2hPcmdMaW5rID8gdGhpcy5zd2l0Y2hPcmdMaW5rIDogJyc7XG4gICAgdGhpcy51c2VyU2V0dGluZ3NMaW5rID0gdGhpcy51c2VyU2V0dGluZ3NMaW5rID8gdGhpcy51c2VyU2V0dGluZ3NMaW5rIDogJyc7XG4gICAgdGhpcy5zaWdub3V0TGluayA9IHRoaXMuc2lnbm91dExpbmsgPyB0aGlzLnNpZ25vdXRMaW5rIDogJyc7XG4gICAgdGhpcy5sb2dpbkxpbmsgPSB0aGlzLmxvZ2luTGluayA/IHRoaXMubG9naW5MaW5rIDogJyc7XG5cbiAgICB0aGlzLl9tZW51U2VydmljZS5hdXRoSG9zdCA9IHRoaXMuYXV0aEhvc3QgPyB0aGlzLmF1dGhIb3N0IDogJyc7XG5cbiAgICBpZiAodGhpcy51c2VyICYmIHRoaXMudXNlci5vcmdhbml6YXRpb24pIHtcbiAgICAgIHRoaXMuX21lbnVTZXJ2aWNlLnVzZXIgPSB0aGlzLnVzZXI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX21lbnVTZXJ2aWNlLnZlcmlmeVVzZXIoKS5zdWJzY3JpYmUoKHJlc3VsdDogQXV0aHJlc3VsdCkgPT4ge1xuICAgICAgICBpZiAocmVzdWx0LnVzZXIgJiYgcmVzdWx0LnVzZXIub3JnYW5pemF0aW9uKSB7XG4gICAgICAgICAgdGhpcy5fbWVudVNlcnZpY2UudXNlciA9IHJlc3VsdC51c2VyO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gdGhpcy5fbWVudVNlcnZpY2UubG9naW5MaW5rO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByb3V0ZSAoZTogYW55LCBsaW5rOiBzdHJpbmcpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBjb25zb2xlLmxvZygnbGluaycsIGxpbmspO1xuXG4gICAgaWYgKGxpbmsuc3Vic3RyKDAsMSkgPT09ICcvJykge1xuICAgICAgbGV0IHdvcmQgPSBsaW5rLnN1YnN0cigxKTtcbiAgICAgIGlmICghdGhpcy5yb3V0ZXIuY29uZmlnIHx8ICh0aGlzLnJvdXRlci5jb25maWcgJiYgIXRoaXMucm91dGVyLmNvbmZpZy5sZW5ndGgpICkge1xuICAgICAgICBjb25zb2xlLmxvZygnbm8gY29uZmlnJyk7XG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gbGluaztcbiAgICAgIH1cblxuICAgICAgZm9yIChsZXQgYSA9IDA7IGEgIT09IHRoaXMucm91dGVyLmNvbmZpZy5sZW5ndGg7IGErKykge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnJvdXRlci5jb25maWdbYV0ucGF0aCwgd29yZCk7XG4gICAgICAgIGlmICh0aGlzLnJvdXRlci5jb25maWdbYV0ucGF0aCA9PT0gd29yZCkge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdtb3ZpbmcgdG8gcm91dGUnKTtcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbd29yZF0pO1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnJvdXRlci5jb25maWdbYV0uY2hpbGRyZW4pIHtcbiAgICAgICAgICBmb3IgKGxldCBiID0gMDsgYiAhPT0gdGhpcy5yb3V0ZXIuY29uZmlnW2FdLmNoaWxkcmVuLmxlbmd0aDsgYSsrKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5yb3V0ZXIuY29uZmlnW2FdLmNoaWxkcmVuW2JdLnBhdGggPT09IHdvcmQgfHwgdGhpcy5yb3V0ZXIuY29uZmlnW2FdLmNoaWxkcmVuW2JdLnJlZGlyZWN0VG8gPT09IHdvcmQpIHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coJ21vdmluZyB0byBjaGlsZCByb3V0ZScpO1xuICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbd29yZF0pO1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGNvbnNvbGUubG9nKCdub25lJyk7XG4gICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGxpbms7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKCdubyBzbGFzaCAnKTtcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gbGluaztcbiAgICB9XG4gIH1cbn1cbiJdfQ==