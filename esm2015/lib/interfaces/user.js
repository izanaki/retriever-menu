/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function RetrieverUser() { }
if (false) {
    /** @type {?} */
    RetrieverUser.prototype.name;
    /** @type {?} */
    RetrieverUser.prototype.email;
    /** @type {?} */
    RetrieverUser.prototype.first_name;
    /** @type {?} */
    RetrieverUser.prototype.last_name;
    /** @type {?} */
    RetrieverUser.prototype.organization;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3JldHJpZXZlci1tZW51LyIsInNvdXJjZXMiOlsibGliL2ludGVyZmFjZXMvdXNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUEsbUNBTUM7OztJQUxDLDZCQUFhOztJQUNiLDhCQUFjOztJQUNkLG1DQUFtQjs7SUFDbkIsa0NBQWtCOztJQUNsQixxQ0FBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1JldHJpZXZlck9yZ2FuaXphdGlvbn0gZnJvbSAnLi9vcmdhbml6YXRpb24nO1xuXG5leHBvcnQgaW50ZXJmYWNlIFJldHJpZXZlclVzZXIge1xuICBuYW1lOiBzdHJpbmc7XG4gIGVtYWlsOiBzdHJpbmc7XG4gIGZpcnN0X25hbWU6IHN0cmluZztcbiAgbGFzdF9uYW1lOiBzdHJpbmc7XG4gIG9yZ2FuaXphdGlvbjogUmV0cmlldmVyT3JnYW5pemF0aW9uO1xufSJdfQ==