import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { Injectable, NgModule, Component, Input, ViewEncapsulation, defineInjectable, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RetrieverMenuService {
    /**
     * @param {?} http
     * @param {?} _cookieService
     */
    constructor(http, _cookieService) {
        this.http = http;
        this._cookieService = _cookieService;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._cookieService.get('token')
            })
        };
    }
    /**
     * @return {?}
     */
    get authHost() { return this._authHost; }
    /**
     * @param {?} authHost
     * @return {?}
     */
    set authHost(authHost) { this._authHost = authHost; }
    /**
     * @return {?}
     */
    get playlistsLink() { return this._playlistsLink; }
    /**
     * @param {?} playlistsLink
     * @return {?}
     */
    set playlistsLink(playlistsLink) { this._playlistsLink = playlistsLink; }
    /**
     * @return {?}
     */
    get templatesLink() { return this._templatesLink; }
    /**
     * @param {?} templatesLink
     * @return {?}
     */
    set templatesLink(templatesLink) { this._templatesLink = templatesLink; }
    /**
     * @return {?}
     */
    get savedTemplatesLink() { return this._savedTemplatesLink; }
    /**
     * @param {?} savedTemplatesLink
     * @return {?}
     */
    set savedTemplatesLink(savedTemplatesLink) { this._savedTemplatesLink = savedTemplatesLink; }
    /**
     * @return {?}
     */
    get mediaLibLink() { return this._mediaLibLink; }
    /**
     * @param {?} mediaLibLink
     * @return {?}
     */
    set mediaLibLink(mediaLibLink) { this._mediaLibLink = mediaLibLink; }
    /**
     * @return {?}
     */
    get appsLink() { return this._appsLink; }
    /**
     * @param {?} appsLink
     * @return {?}
     */
    set appsLink(appsLink) { this._appsLink = appsLink; }
    /**
     * @return {?}
     */
    get devicesLink() { return this._devicesLink; }
    /**
     * @param {?} devicesLink
     * @return {?}
     */
    set devicesLink(devicesLink) { this._devicesLink = devicesLink; }
    /**
     * @return {?}
     */
    get acctSettingsLink() { return this._acctSettingsLink; }
    /**
     * @param {?} acctSettingsLink
     * @return {?}
     */
    set acctSettingsLink(acctSettingsLink) { this._acctSettingsLink = acctSettingsLink; }
    /**
     * @return {?}
     */
    get switchOrgLink() { return this._switchOrgLink; }
    /**
     * @param {?} switchOrgLink
     * @return {?}
     */
    set switchOrgLink(switchOrgLink) { this._switchOrgLink = switchOrgLink; }
    /**
     * @return {?}
     */
    get userSettingsLink() { return this._userSettingsLink; }
    /**
     * @param {?} userSettingsLink
     * @return {?}
     */
    set userSettingsLink(userSettingsLink) { this._userSettingsLink = userSettingsLink; }
    /**
     * @return {?}
     */
    get signoutLink() { return this._signoutLink; }
    /**
     * @param {?} signoutLink
     * @return {?}
     */
    set signoutLink(signoutLink) { this._signoutLink = signoutLink; }
    /**
     * @return {?}
     */
    get loginLink() { return this._loginLink; }
    /**
     * @param {?} loginLink
     * @return {?}
     */
    set loginLink(loginLink) { this._loginLink = loginLink; }
    /**
     * @return {?}
     */
    get authKey() { return this._authKey; }
    /**
     * @param {?} authKey
     * @return {?}
     */
    set authKey(authKey) { this._authKey = authKey; }
    /**
     * @return {?}
     */
    get user() { return this._user; }
    /**
     * @param {?} user
     * @return {?}
     */
    set user(user) { this._user = user; }
    /**
     * @return {?}
     */
    verifyUser() {
        return this.http
            .get(this.authHost + 'verify', this.httpOptions);
    }
}
RetrieverMenuService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
RetrieverMenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: CookieService }
];
/** @nocollapse */ RetrieverMenuService.ngInjectableDef = defineInjectable({ factory: function RetrieverMenuService_Factory() { return new RetrieverMenuService(inject(HttpClient), inject(CookieService)); }, token: RetrieverMenuService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RetrieverMenuComponent {
    /**
     * @param {?} _menuService
     * @param {?} router
     */
    constructor(_menuService, router) {
        this._menuService = _menuService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.tokenKey = this.tokenKey ? this.tokenKey : 'token';
        this.playlistsLink = this.playlistsLink ? this.playlistsLink : '';
        this.templatesLink = this.templatesLink ? this.templatesLink : '';
        this.savedTemplatesLink = this.savedTemplatesLink ? this.savedTemplatesLink : '';
        this.mediaLibLink = this.mediaLibLink ? this.mediaLibLink : '';
        this.devicesLink = this.devicesLink ? this.devicesLink : '';
        this.acctSettingsLink = this.acctSettingsLink ? this.acctSettingsLink : '';
        this.switchOrgLink = this.switchOrgLink ? this.switchOrgLink : '';
        this.userSettingsLink = this.userSettingsLink ? this.userSettingsLink : '';
        this.signoutLink = this.signoutLink ? this.signoutLink : '';
        this.loginLink = this.loginLink ? this.loginLink : '';
        this._menuService.authHost = this.authHost ? this.authHost : '';
        if (this.user && this.user.organization) {
            this._menuService.user = this.user;
        }
        else {
            this._menuService.verifyUser().subscribe((result) => {
                if (result.user && result.user.organization) {
                    this._menuService.user = result.user;
                }
                else {
                    window.location.href = this._menuService.loginLink;
                }
            });
        }
    }
    /**
     * @param {?} e
     * @param {?} link
     * @return {?}
     */
    route(e, link) {
        e.preventDefault();
        console.log('link', link);
        if (link.substr(0, 1) === '/') {
            /** @type {?} */
            let word = link.substr(1);
            if (!this.router.config || (this.router.config && !this.router.config.length)) {
                console.log('no config');
                window.location.href = link;
            }
            for (let a = 0; a !== this.router.config.length; a++) {
                console.log(this.router.config[a].path, word);
                if (this.router.config[a].path === word) {
                    console.log('moving to route');
                    this.router.navigate([word]);
                    return true;
                }
                if (this.router.config[a].children) {
                    for (let b = 0; b !== this.router.config[a].children.length; a++) {
                        if (this.router.config[a].children[b].path === word || this.router.config[a].children[b].redirectTo === word) {
                            console.log('moving to child route');
                            this.router.navigate([word]);
                            return true;
                        }
                    }
                }
            }
            console.log('none');
            window.location.href = link;
        }
        else {
            console.log('no slash ');
            window.location.href = link;
        }
    }
}
RetrieverMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-retriever-menu',
                template: "<head>\n\t<meta charset=\"UTF-8\">\n\t<title>Playlist Manager</title>\n\t<link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500,900\" rel=\"stylesheet\">\n\t<link href=\"https://fonts.googleapis.com/css?family=Jolly+Lodger|Fontdiner+Swanky|Spicy+Rice\" rel=\"stylesheet\">\n</head>\n\n<!-- Site Content -->\n<header class=\"site-header\">\n\t<nav class=\"navbar fixed-top navbar-expand navbar-dark bg-dark\">\n\t\t<a class=\"navbar-brand mr-auto\" [href]=\"loginLink\" (click)=\"route($event, loginLink)\">Retriever</a>\n\t\t<a class=\"nav-link\" href=\"#\"><i class=\"fas fa-question-circle\"></i> Help</a>\n\t\t<!--<a class=\"nav-link dropup-toggle\" href=\"#\">Account</a>-->\n\t\t<input type=\"checkbox\" id=\"nav-account\" class=\"nav-check\">\n\t\t<label for=\"nav-account\" class=\"dropup-toggle\" *ngIf=\"_menuService.user\">\n\t\t\t<span class=\"sr-only\">Menu</span>\n\t\t\t<span class=\"menu-icon\">\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--top\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--middle\"></span>\n\t\t\t\t\t<span class=\"menu-icon__bar menu-icon__bar--bottom\"></span>\n\t\t\t\t</span>\n\t\t</label>\n\t\t<div class=\"dropup-menu\" *ngIf=\"_menuService.user\">\n\t\t\t<div class=\"dropup-menu__content\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"playlistsLink\" (click)=\"route($event, playlistsLink)\" class=\"dropup-item\"><i class=\"fas fa-th-list\"></i> Playlists</a>\n\t\t\t\t\t\t\t\t<a [href]=\"templatesLink\" (click)=\"route($event, templatesLink)\" class=\"dropup-item\"><i class=\"fas fa-th-large\"></i> Templates</a>\n\t\t\t\t\t\t\t\t<a [href]=\"savedTemplatesLink\" (click)=\"route($event, savedTemplatesLink)\" class=\"dropup-item\"><i class=\"fas fa-star\"></i> Saved Templates</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<a [href]=\"mediaLibLink\" (click)=\"route($event, mediaLibLink)\" class=\"dropup-item\"><i class=\"far fa-images\"></i> Media Library</a>\n\t\t\t\t\t\t\t\t<a [href]=\"appsLink\" (click)=\"route($event, appsLink)\" class=\"dropup-item\"><i class=\"fas fa-th\"></i> Apps</a>\n\t\t\t\t\t\t\t\t<a [href]=\"devicesLink\" (click)=\"route($event, devicesLink)\" class=\"dropup-item\"><i class=\"fas fa-server\"></i> Devices</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm border-left-sm\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-sm-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">{{_menuService.user.organization.name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"acctSettingsLink\" (click)=\"route($event, acctSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> Account Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"switchOrgLink\" (click)=\"route($event, switchOrgLink)\"><i class=\"fas fa-exchange-alt\"></i> Switch Organization</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-lg\">\n\t\t\t\t\t\t\t\t<div class=\"dropup-divider d-lg-none\"></div>\n\t\t\t\t\t\t\t\t<span class=\"dropup-header\" href=\"#\">\n\t\t\t\t\t\t\t\t\t<!--<img class=\"avatar\" src=\"images/DTS_005-FACES_CIRCLE_L.png\" alt=\"\"> -->\n\t\t\t\t\t\t\t\t\t{{_menuService.user.first_name}} {{_menuService.user.last_name}}</span>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"userSettingsLink\" (click)=\"route($event, userSettingsLink)\"><i class=\"fas fa-sliders-h\"></i> User Settings</a>\n\t\t\t\t\t\t\t\t<a class=\"dropup-item\" [href]=\"signoutLink\" (click)=\"route($event, signoutLink)\"><i class=\"fas fa-sign-out-alt\"></i> Sign Out</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div><!-- -->\n\t\t\t</div>\n\t\t</div>\n\t</nav>\n</header>\n",
                encapsulation: ViewEncapsulation.None
            }] }
];
/** @nocollapse */
RetrieverMenuComponent.ctorParameters = () => [
    { type: RetrieverMenuService },
    { type: Router }
];
RetrieverMenuComponent.propDecorators = {
    user: [{ type: Input }],
    tokenKey: [{ type: Input }],
    authHost: [{ type: Input }],
    playlistsLink: [{ type: Input }],
    templatesLink: [{ type: Input }],
    savedTemplatesLink: [{ type: Input }],
    mediaLibLink: [{ type: Input }],
    appsLink: [{ type: Input }],
    devicesLink: [{ type: Input }],
    acctSettingsLink: [{ type: Input }],
    switchOrgLink: [{ type: Input }],
    userSettingsLink: [{ type: Input }],
    signoutLink: [{ type: Input }],
    loginLink: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RetrieverMenuModule {
}
RetrieverMenuModule.decorators = [
    { type: NgModule, args: [{
                declarations: [RetrieverMenuComponent],
                imports: [CommonModule, RouterModule.forRoot([])],
                exports: [RetrieverMenuComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { RetrieverMenuService, RetrieverMenuComponent, RetrieverMenuModule };

//# sourceMappingURL=retriever-menu.js.map