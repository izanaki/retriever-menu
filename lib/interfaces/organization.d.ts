export interface RetrieverOrganization {
    account_id: number;
    name: string;
    role: string;
    role_id: number;
}
