/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RetrieverMenuComponent } from './retriever-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
var RetrieverMenuModule = /** @class */ (function () {
    function RetrieverMenuModule() {
    }
    RetrieverMenuModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [RetrieverMenuComponent],
                    imports: [CommonModule, RouterModule.forRoot([])],
                    exports: [RetrieverMenuComponent]
                },] }
    ];
    return RetrieverMenuModule;
}());
export { RetrieverMenuModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcmV0cmlldmVyLW1lbnUvIiwic291cmNlcyI6WyJsaWIvcmV0cmlldmVyLW1lbnUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFN0M7SUFBQTtJQUttQyxDQUFDOztnQkFMbkMsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLHNCQUFzQixDQUFDO29CQUN0QyxPQUFPLEVBQUUsQ0FBRSxZQUFZLEVBQUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBRTtvQkFDbkQsT0FBTyxFQUFFLENBQUMsc0JBQXNCLENBQUM7aUJBQ2xDOztJQUNrQywwQkFBQztDQUFBLEFBTHBDLElBS29DO1NBQXZCLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXRyaWV2ZXJNZW51Q29tcG9uZW50IH0gZnJvbSAnLi9yZXRyaWV2ZXItbWVudS5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbUmV0cmlldmVyTWVudUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFsgQ29tbW9uTW9kdWxlLCBSb3V0ZXJNb2R1bGUuZm9yUm9vdChbXSkgXSxcbiAgZXhwb3J0czogW1JldHJpZXZlck1lbnVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFJldHJpZXZlck1lbnVNb2R1bGUgeyB9XG4iXX0=