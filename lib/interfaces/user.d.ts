import { RetrieverOrganization } from './organization';
export interface RetrieverUser {
    name: string;
    email: string;
    first_name: string;
    last_name: string;
    organization: RetrieverOrganization;
}
