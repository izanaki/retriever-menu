import { OnInit } from '@angular/core';
import { RetrieverMenuService } from './retriever-menu.service';
import { RetrieverUser } from './interfaces/user';
import { Router } from '@angular/router';
export declare class RetrieverMenuComponent implements OnInit {
    _menuService: RetrieverMenuService;
    private router;
    user: RetrieverUser;
    tokenKey: string;
    authHost: string;
    playlistsLink: string;
    templatesLink: string;
    savedTemplatesLink: string;
    mediaLibLink: string;
    appsLink: string;
    devicesLink: string;
    acctSettingsLink: string;
    switchOrgLink: string;
    userSettingsLink: string;
    signoutLink: string;
    loginLink: string;
    isLogged: any;
    constructor(_menuService: RetrieverMenuService, router: Router);
    ngOnInit(): void;
    route(e: any, link: string): boolean;
}
