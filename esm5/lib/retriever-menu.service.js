/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "ngx-cookie";
var RetrieverMenuService = /** @class */ (function () {
    function RetrieverMenuService(http, _cookieService) {
        this.http = http;
        this._cookieService = _cookieService;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._cookieService.get('token')
            })
        };
    }
    Object.defineProperty(RetrieverMenuService.prototype, "authHost", {
        get: /**
         * @return {?}
         */
        function () { return this._authHost; },
        set: /**
         * @param {?} authHost
         * @return {?}
         */
        function (authHost) { this._authHost = authHost; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "playlistsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._playlistsLink; },
        set: /**
         * @param {?} playlistsLink
         * @return {?}
         */
        function (playlistsLink) { this._playlistsLink = playlistsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "templatesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._templatesLink; },
        set: /**
         * @param {?} templatesLink
         * @return {?}
         */
        function (templatesLink) { this._templatesLink = templatesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "savedTemplatesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._savedTemplatesLink; },
        set: /**
         * @param {?} savedTemplatesLink
         * @return {?}
         */
        function (savedTemplatesLink) { this._savedTemplatesLink = savedTemplatesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "mediaLibLink", {
        get: /**
         * @return {?}
         */
        function () { return this._mediaLibLink; },
        set: /**
         * @param {?} mediaLibLink
         * @return {?}
         */
        function (mediaLibLink) { this._mediaLibLink = mediaLibLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "appsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._appsLink; },
        set: /**
         * @param {?} appsLink
         * @return {?}
         */
        function (appsLink) { this._appsLink = appsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "devicesLink", {
        get: /**
         * @return {?}
         */
        function () { return this._devicesLink; },
        set: /**
         * @param {?} devicesLink
         * @return {?}
         */
        function (devicesLink) { this._devicesLink = devicesLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "acctSettingsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._acctSettingsLink; },
        set: /**
         * @param {?} acctSettingsLink
         * @return {?}
         */
        function (acctSettingsLink) { this._acctSettingsLink = acctSettingsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "switchOrgLink", {
        get: /**
         * @return {?}
         */
        function () { return this._switchOrgLink; },
        set: /**
         * @param {?} switchOrgLink
         * @return {?}
         */
        function (switchOrgLink) { this._switchOrgLink = switchOrgLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "userSettingsLink", {
        get: /**
         * @return {?}
         */
        function () { return this._userSettingsLink; },
        set: /**
         * @param {?} userSettingsLink
         * @return {?}
         */
        function (userSettingsLink) { this._userSettingsLink = userSettingsLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "signoutLink", {
        get: /**
         * @return {?}
         */
        function () { return this._signoutLink; },
        set: /**
         * @param {?} signoutLink
         * @return {?}
         */
        function (signoutLink) { this._signoutLink = signoutLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "loginLink", {
        get: /**
         * @return {?}
         */
        function () { return this._loginLink; },
        set: /**
         * @param {?} loginLink
         * @return {?}
         */
        function (loginLink) { this._loginLink = loginLink; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "authKey", {
        get: /**
         * @return {?}
         */
        function () { return this._authKey; },
        set: /**
         * @param {?} authKey
         * @return {?}
         */
        function (authKey) { this._authKey = authKey; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RetrieverMenuService.prototype, "user", {
        get: /**
         * @return {?}
         */
        function () { return this._user; },
        set: /**
         * @param {?} user
         * @return {?}
         */
        function (user) { this._user = user; },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    RetrieverMenuService.prototype.verifyUser = /**
     * @return {?}
     */
    function () {
        return this.http
            .get(this.authHost + 'verify', this.httpOptions);
    };
    RetrieverMenuService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    RetrieverMenuService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: CookieService }
    ]; };
    /** @nocollapse */ RetrieverMenuService.ngInjectableDef = i0.defineInjectable({ factory: function RetrieverMenuService_Factory() { return new RetrieverMenuService(i0.inject(i1.HttpClient), i0.inject(i2.CookieService)); }, token: RetrieverMenuService, providedIn: "root" });
    return RetrieverMenuService;
}());
export { RetrieverMenuService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._authHost;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._playlistsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._templatesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._savedTemplatesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._mediaLibLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._appsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._devicesLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._acctSettingsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._switchOrgLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._userSettingsLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._signoutLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._loginLink;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._authKey;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._user;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype.httpOptions;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    RetrieverMenuService.prototype._cookieService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0cmlldmVyLW1lbnUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3JldHJpZXZlci1tZW51LyIsInNvdXJjZXMiOlsibGliL3JldHJpZXZlci1tZW51LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUM3RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sWUFBWSxDQUFDOzs7O0FBR3pDO0lBaUZFLDhCQUFvQixJQUFnQixFQUFVLGNBQTZCO1FBQXZELFNBQUksR0FBSixJQUFJLENBQVk7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQVBuRSxnQkFBVyxHQUFHO1lBQ3BCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDdkIsY0FBYyxFQUFHLGtCQUFrQjtnQkFDbkMsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7YUFDOUQsQ0FBQztTQUNILENBQUM7SUFFNkUsQ0FBQztJQTNFaEYsc0JBQUksMENBQVE7Ozs7UUFBWixjQUFrQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzs7OztRQUMxQyxVQUFjLFFBQWdCLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDOzs7T0FEcEI7SUFLMUMsc0JBQUksK0NBQWE7Ozs7UUFBakIsY0FBdUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDcEQsVUFBbUIsYUFBcUIsSUFBSSxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUM7OztPQUQ5QjtJQUtwRCxzQkFBSSwrQ0FBYTs7OztRQUFqQixjQUF1QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDOzs7OztRQUNwRCxVQUFtQixhQUFxQixJQUFJLElBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQzs7O09BRDlCO0lBS3BELHNCQUFJLG9EQUFrQjs7OztRQUF0QixjQUE0QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7Ozs7O1FBQzlELFVBQXdCLGtCQUEwQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7OztPQUR4QztJQUs5RCxzQkFBSSw4Q0FBWTs7OztRQUFoQixjQUFzQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDOzs7OztRQUNsRCxVQUFrQixZQUFvQixJQUFJLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQzs7O09BRDVCO0lBS2xELHNCQUFJLDBDQUFROzs7O1FBQVosY0FBa0IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDMUMsVUFBYyxRQUFnQixJQUFJLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQzs7O09BRHBCO0lBSzFDLHNCQUFJLDZDQUFXOzs7O1FBQWYsY0FBcUIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDaEQsVUFBaUIsV0FBbUIsSUFBSSxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUM7OztPQUQxQjtJQUtoRCxzQkFBSSxrREFBZ0I7Ozs7UUFBcEIsY0FBMEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDOzs7OztRQUMxRCxVQUFzQixnQkFBd0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDOzs7T0FEcEM7SUFLMUQsc0JBQUksK0NBQWE7Ozs7UUFBakIsY0FBdUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDcEQsVUFBbUIsYUFBcUIsSUFBSSxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUM7OztPQUQ5QjtJQUtwRCxzQkFBSSxrREFBZ0I7Ozs7UUFBcEIsY0FBMEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDOzs7OztRQUMxRCxVQUFzQixnQkFBd0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDOzs7T0FEcEM7SUFLMUQsc0JBQUksNkNBQVc7Ozs7UUFBZixjQUFxQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDOzs7OztRQUNoRCxVQUFpQixXQUFtQixJQUFJLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQzs7O09BRDFCO0lBS2hELHNCQUFJLDJDQUFTOzs7O1FBQWIsY0FBbUIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDNUMsVUFBZSxTQUFpQixJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQzs7O09BRHRCO0lBSzVDLHNCQUFJLHlDQUFPOzs7O1FBQVgsY0FBaUIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs7Ozs7UUFDeEMsVUFBYSxPQUFlLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDOzs7T0FEbEI7SUFLeEMsc0JBQUksc0NBQUk7Ozs7UUFBUixjQUFjLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Ozs7O1FBQ2xDLFVBQVUsSUFBbUIsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7OztPQURuQjs7OztJQVlsQyx5Q0FBVTs7O0lBQVY7UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyRCxDQUFDOztnQkF0RkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFOTyxVQUFVO2dCQUNWLGFBQWE7OzsrQkFGckI7Q0E0RkMsQUF2RkQsSUF1RkM7U0FwRlksb0JBQW9COzs7Ozs7SUFFL0IseUNBQTBCOzs7OztJQUsxQiw4Q0FBK0I7Ozs7O0lBSy9CLDhDQUErQjs7Ozs7SUFLL0IsbURBQW9DOzs7OztJQUtwQyw2Q0FBOEI7Ozs7O0lBSzlCLHlDQUEwQjs7Ozs7SUFLMUIsNENBQTZCOzs7OztJQUs3QixpREFBa0M7Ozs7O0lBS2xDLDhDQUErQjs7Ozs7SUFLL0IsaURBQWtDOzs7OztJQUtsQyw0Q0FBNkI7Ozs7O0lBSzdCLDBDQUEyQjs7Ozs7SUFLM0Isd0NBQXlCOzs7OztJQUt6QixxQ0FBNkI7Ozs7O0lBSTdCLDJDQUtFOzs7OztJQUVVLG9DQUF3Qjs7Ozs7SUFBRSw4Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnQsIEh0dHBIZWFkZXJzfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0Nvb2tpZVNlcnZpY2V9IGZyb20gJ25neC1jb29raWUnO1xuaW1wb3J0IHtSZXRyaWV2ZXJVc2VyfSBmcm9tICcuL2ludGVyZmFjZXMvdXNlcic7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFJldHJpZXZlck1lbnVTZXJ2aWNlIHtcbiAgLy8gVXNlZCB0byBkaXJlY3QgaHR0cCBjYWxscyB0byBhdXRoIHNlcnZlciAvL1xuICBwcml2YXRlIF9hdXRoSG9zdDogc3RyaW5nO1xuICBnZXQgYXV0aEhvc3QgKCkgeyByZXR1cm4gdGhpcy5fYXV0aEhvc3Q7IH1cbiAgc2V0IGF1dGhIb3N0IChhdXRoSG9zdDogc3RyaW5nKSB7IHRoaXMuX2F1dGhIb3N0ID0gYXV0aEhvc3Q7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfcGxheWxpc3RzTGluazogc3RyaW5nO1xuICBnZXQgcGxheWxpc3RzTGluayAoKSB7IHJldHVybiB0aGlzLl9wbGF5bGlzdHNMaW5rOyB9XG4gIHNldCBwbGF5bGlzdHNMaW5rIChwbGF5bGlzdHNMaW5rOiBzdHJpbmcpIHsgdGhpcy5fcGxheWxpc3RzTGluayA9IHBsYXlsaXN0c0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfdGVtcGxhdGVzTGluazogc3RyaW5nO1xuICBnZXQgdGVtcGxhdGVzTGluayAoKSB7IHJldHVybiB0aGlzLl90ZW1wbGF0ZXNMaW5rOyB9XG4gIHNldCB0ZW1wbGF0ZXNMaW5rICh0ZW1wbGF0ZXNMaW5rOiBzdHJpbmcpIHsgdGhpcy5fdGVtcGxhdGVzTGluayA9IHRlbXBsYXRlc0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfc2F2ZWRUZW1wbGF0ZXNMaW5rOiBzdHJpbmc7XG4gIGdldCBzYXZlZFRlbXBsYXRlc0xpbmsgKCkgeyByZXR1cm4gdGhpcy5fc2F2ZWRUZW1wbGF0ZXNMaW5rOyB9XG4gIHNldCBzYXZlZFRlbXBsYXRlc0xpbmsgKHNhdmVkVGVtcGxhdGVzTGluazogc3RyaW5nKSB7IHRoaXMuX3NhdmVkVGVtcGxhdGVzTGluayA9IHNhdmVkVGVtcGxhdGVzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9tZWRpYUxpYkxpbms6IHN0cmluZztcbiAgZ2V0IG1lZGlhTGliTGluayAoKSB7IHJldHVybiB0aGlzLl9tZWRpYUxpYkxpbms7IH1cbiAgc2V0IG1lZGlhTGliTGluayAobWVkaWFMaWJMaW5rOiBzdHJpbmcpIHsgdGhpcy5fbWVkaWFMaWJMaW5rID0gbWVkaWFMaWJMaW5rOyB9XG5cbiAgLy8gVXNlZCB0byBkaXJlY3QgaHR0cCBjYWxscyB0byBwbGF5bGlzdHMgc2VydmVyIC8vXG4gIHByaXZhdGUgX2FwcHNMaW5rOiBzdHJpbmc7XG4gIGdldCBhcHBzTGluayAoKSB7IHJldHVybiB0aGlzLl9hcHBzTGluazsgfVxuICBzZXQgYXBwc0xpbmsgKGFwcHNMaW5rOiBzdHJpbmcpIHsgdGhpcy5fYXBwc0xpbmsgPSBhcHBzTGluazsgfVxuXG4gIC8vIFVzZWQgdG8gZGlyZWN0IGh0dHAgY2FsbHMgdG8gcGxheWxpc3RzIHNlcnZlciAvL1xuICBwcml2YXRlIF9kZXZpY2VzTGluazogc3RyaW5nO1xuICBnZXQgZGV2aWNlc0xpbmsgKCkgeyByZXR1cm4gdGhpcy5fZGV2aWNlc0xpbms7IH1cbiAgc2V0IGRldmljZXNMaW5rIChkZXZpY2VzTGluazogc3RyaW5nKSB7IHRoaXMuX2RldmljZXNMaW5rID0gZGV2aWNlc0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfYWNjdFNldHRpbmdzTGluazogc3RyaW5nO1xuICBnZXQgYWNjdFNldHRpbmdzTGluayAoKSB7IHJldHVybiB0aGlzLl9hY2N0U2V0dGluZ3NMaW5rOyB9XG4gIHNldCBhY2N0U2V0dGluZ3NMaW5rIChhY2N0U2V0dGluZ3NMaW5rOiBzdHJpbmcpIHsgdGhpcy5fYWNjdFNldHRpbmdzTGluayA9IGFjY3RTZXR0aW5nc0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfc3dpdGNoT3JnTGluazogc3RyaW5nO1xuICBnZXQgc3dpdGNoT3JnTGluayAoKSB7IHJldHVybiB0aGlzLl9zd2l0Y2hPcmdMaW5rOyB9XG4gIHNldCBzd2l0Y2hPcmdMaW5rIChzd2l0Y2hPcmdMaW5rOiBzdHJpbmcpIHsgdGhpcy5fc3dpdGNoT3JnTGluayA9IHN3aXRjaE9yZ0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfdXNlclNldHRpbmdzTGluazogc3RyaW5nO1xuICBnZXQgdXNlclNldHRpbmdzTGluayAoKSB7IHJldHVybiB0aGlzLl91c2VyU2V0dGluZ3NMaW5rOyB9XG4gIHNldCB1c2VyU2V0dGluZ3NMaW5rICh1c2VyU2V0dGluZ3NMaW5rOiBzdHJpbmcpIHsgdGhpcy5fdXNlclNldHRpbmdzTGluayA9IHVzZXJTZXR0aW5nc0xpbms7IH1cblxuICAvLyBVc2VkIHRvIGRpcmVjdCBodHRwIGNhbGxzIHRvIHBsYXlsaXN0cyBzZXJ2ZXIgLy9cbiAgcHJpdmF0ZSBfc2lnbm91dExpbms6IHN0cmluZztcbiAgZ2V0IHNpZ25vdXRMaW5rICgpIHsgcmV0dXJuIHRoaXMuX3NpZ25vdXRMaW5rOyB9XG4gIHNldCBzaWdub3V0TGluayAoc2lnbm91dExpbms6IHN0cmluZykgeyB0aGlzLl9zaWdub3V0TGluayA9IHNpZ25vdXRMaW5rOyB9XG5cbiAgLy8gVXNlZCB0byBkaXJlY3QgaHR0cCBjYWxscyB0byBwbGF5bGlzdHMgc2VydmVyIC8vXG4gIHByaXZhdGUgX2xvZ2luTGluazogc3RyaW5nO1xuICBnZXQgbG9naW5MaW5rICgpIHsgcmV0dXJuIHRoaXMuX2xvZ2luTGluazsgfVxuICBzZXQgbG9naW5MaW5rIChsb2dpbkxpbms6IHN0cmluZykgeyB0aGlzLl9sb2dpbkxpbmsgPSBsb2dpbkxpbms7IH1cblxuICAvLyBLZXkgdG8gaWRlbnRpZnkgdG9rZW4gaW4gY29va2llIC8vXG4gIHByaXZhdGUgX2F1dGhLZXk6IHN0cmluZztcbiAgZ2V0IGF1dGhLZXkgKCkgeyByZXR1cm4gdGhpcy5fYXV0aEtleTsgfVxuICBzZXQgYXV0aEtleSAoYXV0aEtleTogc3RyaW5nKSB7IHRoaXMuX2F1dGhLZXkgPSBhdXRoS2V5OyB9XG5cbiAgLy8gS2V5IHRvIGlkZW50aWZ5IHRva2VuIGluIGNvb2tpZSAvL1xuICBwcml2YXRlIF91c2VyOiBSZXRyaWV2ZXJVc2VyO1xuICBnZXQgdXNlciAoKSB7IHJldHVybiB0aGlzLl91c2VyOyB9XG4gIHNldCB1c2VyICh1c2VyOiBSZXRyaWV2ZXJVc2VyKSB7IHRoaXMuX3VzZXIgPSB1c2VyOyB9XG5cbiAgcHJpdmF0ZSBodHRwT3B0aW9ucyA9IHtcbiAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMuX2Nvb2tpZVNlcnZpY2UuZ2V0KCd0b2tlbicpXG4gICAgfSlcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsIHByaXZhdGUgX2Nvb2tpZVNlcnZpY2U6IENvb2tpZVNlcnZpY2UpIHsgfVxuXG4gIHZlcmlmeVVzZXIgKCkge1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5nZXQodGhpcy5hdXRoSG9zdCArICd2ZXJpZnknLCB0aGlzLmh0dHBPcHRpb25zKTtcbiAgfVxufVxuIl19