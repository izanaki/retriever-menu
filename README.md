# RetrieverMenu

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Installation 
 - add module to module that declares component via import { RetrieverMenuModule } from 'retriever-menu';
 - Add declaration to HTML page (See example)

## Example
````
<lib-retriever-menu
	authHost="http://r3-dev-robert.drminc.com:9003/api/v1/"
	playlistsLink="http://r3-dev-robert.drminc.com:"
	templatesLink="http://r3-dev-robert.drminc.com:4201"
	savedTemplatesLink="http://r3-dev-robert.drminc.com:"
	mediaLibLink="/assets"
	appsLink="http://r3-dev-robert.drminc.com:"
	devicesLink="http://r3-dev-robert.drminc.com:"
	acctSettingsLink="acct"
	switchOrgLink="/switch"
	userSettingsLink="/settings"
	signoutLink="http://r3-dev-robert.drminc.com:"
	loginLink="http://r3-dev-robert.drminc.com:4203"
></lib-retriever-menu>
````
